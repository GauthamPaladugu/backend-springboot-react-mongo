package com.example.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode

public class User {

	public String userid;
	public String firstName;
	public String lastName;

	public User() {
		
		System.out.println("Inside No Arg Constructor");
	}

	public User(String firstName, String lastName,String userid) {
		
		System.out.println("Inside Parameterized Constructor" +userid);
		this.firstName = firstName;
		this.lastName = lastName;
		this.userid = userid ;
	}

}

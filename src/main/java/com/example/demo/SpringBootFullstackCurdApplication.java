package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.example.dao.UserRepository;
import com.example.model.User;

@SpringBootApplication
@ComponentScan ({"com.example.controller", "com.example.model"})
@EnableMongoRepositories ("com.example.dao") // this fix the problem

public class SpringBootFullstackCurdApplication implements CommandLineRunner {
	
	@Autowired
	private UserRepository userrepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootFullstackCurdApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("In Run Method of Command Line Runner");
		//userrepository.save(new User("Alice", "Smith"));
	}

}

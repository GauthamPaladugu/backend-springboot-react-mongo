package com.example.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.UserRepository;
import com.example.model.User;

@RestController
@CrossOrigin(origins = { "http://localhost:3000" })
@RequestMapping("/")
public class UserController {

	private final UserRepository userRepository;

	
	 	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		System.out.println("Inside getAllUsers");
		return userRepository.findAll();
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public Optional<User> getUser(@PathVariable String userId) {
		
		System.out.println("Getting user with ID: {}." + userId);		
		return userRepository.findById(userId);
	}

	@RequestMapping(value= "/create" , method = RequestMethod.POST , consumes = MediaType.ALL_VALUE)
	public User addNewUsers(@RequestBody User user)
	{
		System.out.println("Saving User into DB");
		System.out.println("user id >>"+ user.userid);
		System.out.println("firstName"+user.firstName);
		System.out.println("firstName"+user.lastName);
		
		return userRepository.save(user);
	}
	
	/*@RequestMapping(value= "/{user_id}" , method = RequestMethod.POST , consumes = MediaType.ALL_VALUE)
	public User findByUserId(@PathVariable String userId)
	{
		System.out.println("Retriving by User Id");
		System.out.println("user id >>"+ userid);
		return userRepository.findByUserId();
	}
	*/
		
	}